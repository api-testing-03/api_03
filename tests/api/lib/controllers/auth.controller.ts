import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
    // Метод для входу
    async login(emailValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: emailValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
