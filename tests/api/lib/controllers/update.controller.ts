import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class UpdateUserController {
    async updateUser(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();

        return response; // Переконайтеся, що ця відповідь містить код статусу
    }
}
