import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async newPosts(authorIdValue: number, previewImageValue: string, bodyValue: string, AccessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: authorIdValue,
                previewImage: previewImageValue,
                body: bodyValue,
            })
            .bearerToken(AccessToken)
            .send();
        return response;
    }

    async likePosts(entityIdValue: number, isLikeValue: boolean, userIdValue: number, AccessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: entityIdValue,
                isLike: isLikeValue,
                userId: userIdValue,
            })
            .bearerToken(AccessToken)
            .send();
        return response;
    }
}