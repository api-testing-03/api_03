import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class DetailsController {
    async details(accessToken: string, idValue: number, avatarValue: string, emailValue: string, userNameValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .headers({
                Authorization: `Bearer ${accessToken}`,
            })
            .send();

        return response;
    }
}
