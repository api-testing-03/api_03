import { ApiRequest } from "../request";

const baseUrl:string = "http://tasque.lol/";

export class RegController {
    async register(idValue: number, avatarValue: string, emailValue: string, userNameValue: string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                id: idValue,
                avatar: avatarValue,
                email: emailValue,
                userName: userNameValue,
                password: passwordValue,
            })
            .send();
        return response;
    }
}
