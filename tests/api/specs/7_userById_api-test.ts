import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { registeredUser } from "../specs/1_register_api-test";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const schemas = require("./data/schemasUserSearch_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe(`Users ID controller`, () => {
    let userId: number;

    before(async () => {
        userId = registeredUser.user.id;
    });

    it("should return user details with registered user ID", async () => {
        userId = registeredUser.user.id;

        let response = await users.getUserById(userId);

        console.log("User details:");
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_userSearch);
    });
});
