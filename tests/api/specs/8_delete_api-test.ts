import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const chai = require("chai");

xdescribe("Delete User by ID", () => {
    let userAccessToken: string;
    let userId: number;

    before(async () => {
        let loginResponse = await auth.login("vira111@ukr.net", "vira1123441");
        userAccessToken = loginResponse.body.token.accessToken.token;

        let userSearchResponse = await users.searchUserByToken(userAccessToken);
        userId = userSearchResponse.body.id;
    });

    it("should delete user with registered user ID", async () => {
        expect(userAccessToken, "userAccessToken should be defined").to.not.be.undefined;

        let response = await users.deleteUserById(userId, userAccessToken);

        console.log("Delete user response:");
        console.log(response.body);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
    });
});
