import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const chai = require("chai");

describe("Delete User by ID", () => {
    let userAccessToken: string;

    before(async () => {
        let loginResponse = await auth.login("7771@example.com", "mouse12311");
        userAccessToken = loginResponse.body.token.accessToken.token;
    });

    it("should delete user with specified user ID", async () => {
        expect(userAccessToken, "userAccessToken should be defined").to.not.be.undefined;

        const userIdToDelete = 12345; // Конкретний ідентифікатор користувача для видалення

        let response = await users.deleteUserById(userIdToDelete, userAccessToken);

        if (response.statusCode === 204) {
            console.log("User deleted successfully");
            console.log("Delete user response:");
            console.log(response.body);
            checkResponseTime(response, 1000);
        } else {
            console.log("User deletion failed");
        }
    });
});
