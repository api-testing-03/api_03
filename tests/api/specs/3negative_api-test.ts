import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";

const auth = new AuthController();
const users = new UsersController();
const posts = new PostsController();

xdescribe(`Adding likes to posts`, () => {
    let userAccessToken: string;
    let userId: number;

    before(async () => {
        let loginResponse = await auth.login("vira2211@ukr.net", "str22222222ing");
        userAccessToken = loginResponse.body.token.accessToken.token;

        console.log("User Access Token:", userAccessToken);
        let userSearchResponse = await users.searchUserByToken(userAccessToken);
        userId = userSearchResponse.body.id;

        console.log("User ID:", userId);
    });

    it("should add like to a post", async () => {
        expect(userAccessToken, "userAccessToken should be defined").to.not.be.undefined;

        let allPostsResponse = await posts.getAllPosts();
        let allPosts = allPostsResponse.body;

        const postToAddLike = allPosts[0];

        let likeResponse = await posts.likePosts(postToAddLike.id, true, userId, userAccessToken);

        console.log("Full response for adding like to post:", likeResponse.body);

        if (likeResponse.body.success !== undefined) {
            console.log("The 'success' field exists in the response.");
            expect(likeResponse.body.success, "'success' should not be false").to.not.be.false;
        } else {
            console.log("The 'success' field does not exist in the response.");
        }

        console.log("Like added successfully");
        console.log("Response body for adding like to post:");
        console.log(likeResponse.body);
    });
});
