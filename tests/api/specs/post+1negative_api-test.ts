import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new AuthController();
const users = new UsersController();
const posts = new PostsController();
const schemas = require("./data/schemasAllPosts_testData.json");
const newPostSchema = require("./data/schemasNewPost_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe(`Users controller`, () => {
    let userAccessToken: string;
    let userId: number;

    before(async () => {
        let loginResponse = await auth.login("vira1011@ukr.net", "vira1123441");
        userAccessToken = loginResponse.body.token.accessToken.token;

        let userSearchResponse = await users.searchUserByToken(userAccessToken);
        userId = userSearchResponse.body.id;
    });

    it("should return all posts when getting the post collection", async () => {
        let response = await posts.getAllPosts();

        console.log("All posts:");
        console.log(response.body);
    });

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await posts.getAllPosts();

        console.log("Response body:", response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);

        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts);
    });

    it("should add new post", async () => {
        expect(userAccessToken, "userAccessToken should be defined").to.not.be.undefined;

        let response = await posts.newPosts(userId, "string", "This is the body of the post", userAccessToken);

        console.log("Response body for new post:");
        console.log(response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
        expect(response.body).to.be.jsonSchema(newPostSchema.schema_newPost);
    });

    it("should return all posts when getting the post collection", async () => {
        let response = await posts.getAllPosts();

        console.log("All posts:");
        console.log(response.body);
    });

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await posts.getAllPosts();

        console.log("Response body:", response.body);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);

        expect(response.body).to.be.jsonSchema(schemas.schema_allPosts);
    });

    it("should add like to post", async () => {
        expect(userAccessToken, "userAccessToken should be defined").to.not.be.undefined;

        let newPostResponse = await posts.newPosts(
            userId,
            "string",
            "This is the body of the post",
            userAccessToken
        );
        let newPostId = newPostResponse.body.id;

        let likeResponse = await posts.likePosts(newPostId, true, userId, userAccessToken);

        if (likeResponse.statusCode === 200) {
            console.log("Like added successfully");
            console.log("Response body for adding like to post:");
            console.log(likeResponse.body);
            checkResponseTime(likeResponse, 2000);
        }
    });

    it("should return all posts when getting the post collection", async () => {
        let response = await posts.getAllPosts();

        console.log("All posts:");
        console.log(response.body);
    });

    afterEach(() => {
        console.log("it was a test");
    });
});
