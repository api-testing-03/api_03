import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const schemas = require("./data/schemasAllUsers_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

xdescribe(`Users controller`, () => {
    let userId: number;

    it("should return all users when getting the user collection", async () => {
        let response = await users.getAllUsers();

        console.log("All users:");
        console.log(response.body);
    });

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);

        userId = response.body[1].id;
    });
});
