import {
    checkResponseTime,
    checkStatusCode, 
} from '../../helpers/functionsForChecking.helper';
import { RegController } from '../lib/controllers/register.controller';
const reg = new RegController();

describe('Use test data set for registration', () => {
    let parametersDataSet = [
        { id: 0, avatar: 'string', email: 'cat@gmail.com', userName: 'Cat', password: 'food' },
        { id: 0, avatar: 'string', email: '1@gmail.com', userName: 'Cat', password: 'food' },
        { id: 0, avatar: 'string', email: 'catthatlovesfoodcatthatlovesfoodcatthatlovesfoodcatthatlovesfood@gmail.com', userName: 'Cat', password: 'food' },
        { id: 0, avatar: 'string', email: 'cat@ukr.net', userName: 'catthatlovesfood', password: 'foodandmilk' },
        { id: 0, avatar: 'string', email: 'cat@gmail.com', userName: 'Cat', password: 'foodandmilk1234!' },
        { id: 0, avatar: 'string', email: 'cat@gmail.com', userName: 'Cat1!', password: 'food' },
    ];

    parametersDataSet.forEach((parameters) => {
        it(`should register with valid parameters : '${parameters.id}' + '${parameters.avatar}' + '${parameters.email}' + '${parameters.userName}' + '${parameters.password}'`, async () => {
            let response = await reg.register(parameters.id, parameters.avatar, parameters.email, parameters.userName, parameters.password);

            checkStatusCode(response, 201); 
            checkResponseTime(response, 3000);
        });
    });
});

