import { expect } from "chai";
import { RegController } from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const schemas = require("./data/schemasRegister_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

const reg = new RegController();
let registeredUser;

describe("User Registration", () => {
    it("should successfully register a new user", async () => {
        const idValue = 0;
        const avatarValue = "string";
        const emailValue = "7771@example.com";
        const userNameValue = "7771";
        const passwordValue = "mouse12311";

        const response = await reg.register(idValue, avatarValue, emailValue, userNameValue, passwordValue);
        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);

        registeredUser = response.body;
        console.log(registeredUser);

        expect(response.body).to.be.jsonSchema(schemas.schema_register);
        const userId = response.body.user.id;
        console.log(response.body);
    });
});

export { registeredUser };
