import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { DetailsController } from "../lib/controllers/detailsToken.controller";
import { UpdateUserController } from "../lib/controllers/update.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const schemas = require("./data/schemasUserSearch_testData.json");

const auth = new AuthController();
const users = new UsersController();
const userDetails = new DetailsController();
const updateController = new UpdateUserController();
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Get details of the current logged in user", () => {
    let userAccessToken: string;
    let userId: number;
    let userAvatar: string;
    let userName: string;

    before(`Login and get the token`, async () => {
        let loginResponse = await auth.login("cattt@ukr.net", "afdhfgdhd");
        userAccessToken = loginResponse.body.token.accessToken.token;
    });

    it(`should return user details with registered token`, async () => {
        let userSearchResponse = await users.searchUserByToken(userAccessToken);

        console.log("User search response:", userSearchResponse.body);
        expect(userSearchResponse.body).to.be.jsonSchema(schemas.schema_userSearch);

        let userDetailsResponse = await userDetails.details(
            userAccessToken,
            userSearchResponse.body.id,
            userSearchResponse.body.avatar,
            userSearchResponse.body.email,
            userSearchResponse.body.userName
        );

        console.log("User details response:", userDetailsResponse.body);
        userId = userSearchResponse.body.id;
        userAvatar = userSearchResponse.body.avatar;
        userName = userSearchResponse.body.userName;
    });

    it(`should update email of the current user`, async () => {
        let userSearchResponse = await users.searchUserByToken(userAccessToken);

        console.log("User search response:", userSearchResponse.body);
        expect(userSearchResponse.body).to.be.jsonSchema(schemas.schema_userSearch);

        const updatedUser = {
            id: userId,
            avatar: "string",
            email: "cattt@ukr.net",
            userName: "cattt",
        };

        const updateResponse = await updateController.updateUser(updatedUser, userAccessToken);

        console.log("Update user response:", updateResponse.body);

        checkStatusCode(updateResponse, 204);
        checkResponseTime(updateResponse, 1000);
    });

    it(`should return updated user details with registered token`, async () => {
        let userSearchResponse = await users.searchUserByToken(userAccessToken);

        console.log("User search response:", userSearchResponse.body);
        expect(userSearchResponse.body).to.be.jsonSchema(schemas.schema_userSearch);

        let userDetailsResponse = await userDetails.details(
            userAccessToken,
            userSearchResponse.body.id,
            userSearchResponse.body.avatar,
            userSearchResponse.body.email,
            userSearchResponse.body.userName
        );

        console.log("User details response:", userDetailsResponse.body);
        userId = userSearchResponse.body.id;
        userAvatar = userSearchResponse.body.avatar;
        userName = userSearchResponse.body.userName;
    });
});
